from BTrees.OOBTree import OOBTree
# a Btree object creates, inserts, searches and deletes index and value pair in the Btree.
class BTree:
    # __init__ function - Creating and initialising a Btree.
    # @param self - reference of the Btree.
    # @param field - the field in the table whose indices need to be stored in the Btree.
    # @param table_ - the array table whose particular field's indices need to be stored in the Btree.
    def __init__(self, field, table_):
        self. field = field
        self.data = OOBTree()
        header = table_.fields.index(field)
        table_data = table_.data
        for index,row in enumerate(table_data):
            self.insert(row[header], index)

    # insert function - Here the a new index value pair is inserted into the Btree.
    # @param tree - The existing Btree where the search operation must be executed.
    # @param key - The index that needs to be inserted.
    # @param key - The value that needs to be inserted.
    # Btree is appended with the value.
    def insert(self, key, value):
        if key in self.data:
            self.data[key].append(value)
        else:
            self.data[key] = [value]

    # search function - Here the desired key's index value is pulled out from the btree, which then gets the value from the lists.
    # @param tree - The existing Btree where the search operation must be executed.
    # @param key - The key that needs to be searched.
    # @return - The value of the key in the array table.
    # Btree is appended with the value that it was searched for.
    def search(self, key):
        if key in self.data:
            return self.data[key]
        else:
            return []
    
    # delete function - Here the respective key along with the index value is deleted from the btree.
    # @param tree - The existing Btree where the delete operation must be executed.
    # @param key - The key that needs to be deleted.
    # @return - The tree after deletion.
    # The Btree does not have the key anymore.
    def delete(self, key):
        del self.data[key]

    # items function - returns a list of all the items in the Btree.
    # @param self - reference to the Btree.
    # @returns The list of all items in the Btree.
    def items(self):
        return list(self.data.items())