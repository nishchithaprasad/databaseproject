import table
import condition_parser
import operator as operator_
import itertools

# inputfromfile function - Inputs the data from the file 
# and creates an array table.
# @param namespace - The object in which the table will be stored.
# @param file_ - File from which the data will be taken.
# @returns table.
def inputfromfile(namespace, file_):
    table_ = table.Table()
    table_.load(f"input_files/{file_}.csv")
    return table_

# outputtofile function - Returns the dump function where the output will be written on to the output file.
# @param namespace - The object in which the table is stored.
# @param file_ - File to which the result is written.
# @returns dump function in which the outputs will be written.
def outputtofile(namespace, table_name, file_):
    return namespace[table_name].dump(f"output_files/nhv215_as13593_{file_}")

# project function - Returns the projection function in Table object.
# @param namespace - The object in which the table is stored.
# @param table_name - The table name upon which the projection function needs to be performed.
# @param *fields - The fields up on which projection needs to happen.
# @returns projection function in Table object.
def project(namespace, table_name, *fields):
    return namespace[table_name].project(*fields)

# concat function - Returns the concatenation function of two tables in Table object.
# @param namespace - The object in which the table is stored.
# @param table_name_left - The table name upon which the concatenation needs to happen.
# @param table_name_right - The table name upon which the concatenation needs to happen.
# @returns concatenation function in Table object.
def concat(namespace, table_name_left, table_name_right):
    return namespace[table_name_left].concat(
        namespace[table_name_right])

# avg function - Returns the average function in Table object.
# @param namespace - The object in which the table is stored.
# @param table_name - The table name upon which the average function needs to be performed.
# @param fields - The field up on which average function needs to be performed.
# @returns average function in Table object.
def avg(namespace, table_name, field):
    return namespace[table_name].avg(field)

# count function - Returns the count function in Table object.
# @param namespace - The object in which the table is stored.
# @param table_name - The table name upon which the count function needs to be performed.
# @returns count function in Table object.
def count(namespace, table_name):
    return namespace[table_name].count()

# sum function - Returns the sum function in Table object.
# @param namespace - The object in which the table is stored.
# @param table_name - The table name upon which the average function needs to be performed.
# @param fields - The field up on which sum function needs to be performed.
# @returns sum function in Table object.
def sum(namespace, table_name, field):
    return namespace[table_name].sum(field)

# sumgroup function - Returns the sumgroup function of a field in the set of group fields in the Table object.
# @param namespace - The object in which the table is stored.
# @param table_name - The table name upon which the sumgroup function needs to be performed.
# @param sum_field - The field up on which sum needs to be performed.
# @param *grp_fields - The fields up on which grouping needs to be performed.
# @returns sumgroup function in Table object.
def sumgroup(namespace, table_name, sum_field, *grp_fields):
    return namespace[table_name].sumgroup(sum_field, *grp_fields)

# countgroup function - Returns the countgroup function of a field in the set of group fields in the Table object.
# @param namespace - The object in which the table is stored.
# @param table_name - The table name upon which the countgroup function needs to be performed.
# @param *grp_fields - The fields up on which grouping needs to be performed.
# @returns countgroup function in Table object.
def countgroup(namespace, table_name, *grp_fields):
    return namespace[table_name].countgroup(*grp_fields)

# avggroup function - Returns the avggroup function of a field in the set of group fields in the Table object.
# @param namespace - The object in which the table is stored.
# @param table_name - The table name upon which the avggroup function needs to be performed.
# @param avg_field - The field up on which average needs to be performed.
# @param *grp_fields - The fields up on which grouping needs to be performed.
# @returns avggroup function in Table object.
def avggroup(namespace, table_name, avg_field, *grp_fields):
    return namespace[table_name].avggroup(avg_field, *grp_fields)

# select function - Returns the select function in the Table object.
# @param namespace - The object in which the table is stored.
# @param table_name - The table name upon which the avggroup function needs to be performed.
# @param condtion - The condition up on which selection needs to be performed.
# @returns select function in Table object.
def select(namespace, table_name, condition):
    return namespace[table_name].select(condition)

# join function - Returns the result of join operation.
# @param namespace - The object in which the table is stored.
# @param table_name_left - The table name upon which the join needs to happen.
# @param table_name_right - The table name upon which the join needs to happen.
# @param condtion - The condition up on which join needs to be performed.
# @returns join operarion result.
def join(namespace, table_name_left, table_name_right, condition_str):
    # assumption: rel_op is always "and" for join
    result = []

    _, conditions = condition_parser.parse_condition(condition_str)
    
    # flip conditions if left table is the right operand
    for idx, condition in enumerate(conditions):
        _, left_operand, right_operand = condition
        if left_operand.table == table_name_right:
            conditions[idx] = condition_parser.flip_condition(condition)

    left_table = namespace[table_name_left]
    right_table = namespace[table_name_right]
    
    brute_conditions = []
    index_eq_conditions = []
    for condition in conditions:
        comp_op, left_operand, right_operand = condition
        if comp_op == operator_.eq and \
            (left_operand.field in left_table.indexes \
                or right_operand.field in right_table.indexes):
            index_eq_conditions.append(condition)
        else:
            brute_conditions.append(condition)
    
    search_space = None # all rows x all rows
    for condition in index_eq_conditions:
        satisfying_rows = set()
        _, left_operand, right_operand = condition
        if left_operand.field in left_table.indexes:
            # iterate on right
            right_field_idx = right_table.fields.index(right_operand.field)
            for right_row_idx, right_row in enumerate(right_table.data):
                right_val = right_row[right_field_idx]
                left_val = condition_parser.invert_arith(left_operand.arith_op)(
                    right_operand.arith_op(right_val, right_operand.arith_val), left_operand.arith_val)
                left_row_idxs = left_table.indexes[left_operand.field].search(left_val)
                for left_row_idx in left_row_idxs:
                    satisfying_rows.add((left_row_idx, right_row_idx))
        elif right_operand.field in right_table.indexes:
            #iterate on left
            left_field_idx = left_table.fields.index(left_operand.field)
            for left_row_idx, left_row in enumerate(left_table.data):
                left_val = left_row[left_field_idx]
                right_val = condition_parser.invert_arith(right_operand.arith_op)(
                    left_operand.arith_op(left_val, left_operand.arith_val), right_operand.arith_val)
                right_row_idxs = right_table.indexes[right_operand.field].search(right_val)
                for right_row_idx in right_row_idxs:
                    satisfying_rows.add((left_row_idx, right_row_idx))

        # take intersection of satisfying_rows with search_space
        if search_space is None:
            search_space = satisfying_rows
        else:
            search_space.intersection_update(satisfying_rows)

    if search_space is None:
        search_space = itertools.product(range(len(left_table.data)), range(len(right_table.data)))

    for left_row_idx, right_row_idx in search_space:

        ok = True
        for condition in brute_conditions:
            comp_op, left_operand, right_operand = condition
            left_row = left_table.data[left_row_idx]
            right_row = right_table.data[right_row_idx]
            # compare the left_operand to right_operand
            if not comp_op(left_operand.arith_op(
                left_row[left_table.fields.index(left_operand.field)], left_operand.arith_val),
                right_operand.arith_op(right_row[right_table.fields.index(right_operand.field)], right_operand.arith_val)):

                ok = False
                break

        if ok:
            result.append(left_row + right_row)

    result_fields = [f"{table_name_left}_{field}" for field in left_table.fields] + \
        [f"{table_name_right}_{field}" for field in right_table.fields]

    return table.Table(result_fields, result)


# sort function - Returns the sort function of a field in the Table object.
# @param namespace - The object in which the table is stored.
# @param table_name - The table name upon which the avggroup function needs to be performed.
# @param field - The fields up on which the table needs to be sorted.
# @returns sort function in Table object.
def sort(namespace, table_name, *fields):
    return namespace[table_name].sort(*fields)

# movavg function - Returns the moving average function of a field and length in the Table object.
# @param namespace - The object in which the table is stored.
# @param table_name - The table name upon which the avggroup function needs to be performed.
# @param field - The field up on which moving average needs to be performed.
# @param length - The number of items on which average needs to be taken.
# @returns avggroup function in Table object.
def movavg(namespace, table_name, field, length):
    return namespace[table_name].movavg(field, length)

# movsum function - Returns the moving sum function of a field and length in the Table object.
# @param namespace - The object in which the table is stored.
# @param table_name - The table name upon which the moving sum function needs to be performed.
# @param field - The field up on which moving sum needs to be performed.
# @param length - The number of items on which sum needs to be calculated.
# @returns movsum function in Table object.
def movsum(namespace, table_name, field, length):
    return namespace[table_name].movsum(field, length)

# Btree function - Returns a function in Table object which initialises a BTree with indexes of the field.
# @param namespace - The object in which the table is stored.
# @param table_name - The table name upon which the moving sum function needs to be performed.
# @param field - The field up on which the indices needs to be stored.
# @returns the function in Table object which initialises a BTree with indexes of the field.
def Btree(namespace, table_name, field):
    return namespace[table_name].Btree(field)

# Hash function - Returns a function in Table object which initialises a HashTable with indexes of the field.
# @param namespace - The object in which the table is stored.
# @param table_name - The table name upon which the moving sum function needs to be performed.
# @param field - The field up on which the indices needs to be stored.
# @returns the function in Table object which initialises a HashTable with indexes of the field.
def Hash(namespace, table_name, field):
    return namespace[table_name].Hash(field)
