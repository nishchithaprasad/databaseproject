# parser for conditions

import table
from collections import OrderedDict
import operator as operator_

# dictionary of all relational operators.
comparison_operators = OrderedDict({
    ">=": operator_.ge,
    "<=": operator_.le,
    "!=": operator_.ne,
    "<": operator_.lt,
    ">": operator_.gt,
    "=": operator_.eq,
})

# dictionary of all arithmetic operators.
arithmetic_operators = OrderedDict({
    "+": operator_.add,
    "-": operator_.sub,
    "*": operator_.mul,
    "/": operator_.truediv,
})

# list of all logical operators.
logical_operators = [ "and", "or" ]

# parse_condition function - parses the condition string.
# @param condition - string of all the conditions together.
# @returns logical operator and a list of parsed individual condition.
def parse_condition(condition):
    # returns (relop, [(comp_op, operand, operand)])

    condition = condition.strip()
    for operator in logical_operators:
        if operator in condition:
            conditions = condition.split(operator)
            logical_op = operator
            break
    else: #nobreak
        conditions = [condition]
        logical_op = None
    
    return logical_op, [parse_single_condition(condition) for condition in conditions]

# parse_single_condition function - parses individual condition.
# @param condition - Individual condition that needs to be parsed.
# @returns conditional operator, the parsed left side operator and right side operator.
def parse_single_condition(condition):
    # cannot have logical operators
    # might have table.attr or attr
    # might have arithmetic operators
    # might have ()
    condition = condition.replace("(","").replace(")","")
    for operator in comparison_operators:
        if operator in condition:
            left_operand, right_operand = condition.split(operator)
            condition_operator = comparison_operators[operator]
            break
    left_operand = parse_operand(left_operand.strip())
    right_operand = parse_operand(right_operand.strip())
    return condition_operator, left_operand, right_operand

# flip_condition - When an operator is moved ot the other side of the equality, it needs to be flipped.
# condtion - The condition that needs to be flipped.
# @returns the inverted relational operator and initial left operand is returned as right and vice-versa.
def flip_condition(condition):
    comp_op, left_operand, right_operand = condition
    inverted_comp_op = {
        operator_.ge: operator_.lt,
        operator_.le: operator_.gt,
        operator_.gt: operator_.le,
        operator_.lt: operator_.ge,
        operator_.eq: operator_.eq,
    }[comp_op]
    return inverted_comp_op, right_operand, left_operand

# flip_condition - When an operator is moved ot the other side of the equality, it needs to be flipped.
# arith_op - The arithmetic operator that needs to be flipped.
# @returns the inverted arithmetic operator.
def invert_arith(arith_op):
    return {
        operator_.add: operator_.sub,
        operator_.sub: operator_.add,
        operator_.mul: operator_.truediv,
        operator_.truediv: operator_.mul,
    }[arith_op]

# Class Value is used when there is only a constant on one side of the operand.
class Value:

    # __init__ function - Initialises a new Value object with the value.
    # @param self -  the reference of the new object created now.
    # @param val - the value of the constant.
    def __init__(self, val):
        self.val = val

    # __repr__ function - Returns the value of the constant.
    # param self -  the reference of the object.
    # @returns the string of the val.
    def __repr__(self):
        return f"Value({self.val})"

# Class Field is used when there are table, field, arithmetic operator with value. One or more might not be present too.
class Field:

    # __init__ function - Initialises a new Field object with the appropriate values.
    # @param self -  the reference of the new object created now.
    # @param table_ -  The table name up on which the operation needs to happen.
    # @param field - The field in the table that needs to be compared.
    # @arith_op - The arithmetic operator in this side of the operand.
    # @arith_val - The arithmetic value in this side of the operand.
    def __init__(self, table_, field, arith_op, arith_val):
        self.table = table_
        self.field = field
        self.arith_op = arith_op
        self.arith_val = table._conv_numeric(arith_val)

    # __repr__ function - Returns the entire Field object in string format.
    # param self -  the reference of the object.
    # @returns the string representation of the object.
    def __repr__(self):
        return f"Field(table={self.table}, field={self.field}, op={self.arith_op}, val={self.arith_val})"

# parse_operand - parses each operand.
# @param operand - the operand that needs to be parsed.
# @returns the parsed operand.
def parse_operand(operand):
    # [table.]attr [arithop arithval] | value
    operand = operand.strip()
    if operand.isdigit():
        operand = table._conv_numeric(operand)
        return Value(operand)
    elif '"' in operand or "'" in operand:
        operand = operand.strip()[1:-1]


    operands = operand.split()

    table_field = operands[0]
    if "." in table_field:
        table_, field = table_field.split(".")
    else:
        table_ = None
        field = table_field

    if len(operands) > 1:
        arith_op = arithmetic_operators[operands[1]]
        arith_val = table._conv_numeric(operands[2])
    else:
        arith_op = operator_.add
        arith_val = 0

    return Field(table_, field, arith_op, arith_val)


if __name__ == "__main__":
    print(parse_condition("R1.time + 5 > (S.time + 5)"))
    print(parse_condition("qty + 5 > 20"))
    print(parse_condition("20 < qty + 5"))
    print(parse_condition("R.qty = R1.qty"))   
    print(parse_condition("(time > 50) or (qty < 30)"))
    print(parse_condition(" (R1.qty > S.Q) and (R1.saleid = S.saleid)"))
    print(parse_condition(" (R1.qty + 5 > S.Q / 20) and (R1.saleid - 40 = S.saleid * 80) and (R1.saleid - 40 = S.saleid * 80)"))