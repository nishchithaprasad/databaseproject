from operation import Operation

# parse_line function - Each line in the commands file is parsed 
# and operation object is called with each command.
# @param line - Command to be parsed.
# @return Operation object with the table name, function to be performed and the arguments required for that function.
# the object Operation contains the table name, function to be performed and the arguments required for the same.
def parse_line(line):
    # remove comments
    line = line.split("//", 1)[0].strip()
    if not line:
        return None

    if ":=" in line:
        var, func_parts = line.split(":=")
        var = var.strip()
        func_parts = func_parts.strip()
    else:
        var = None
        func_parts = line

    func, rest = func_parts.split("(", 1)
    func = func.strip()
    args = rest.strip()[:-1].split(",")
    args = [arg.strip() for arg in args]

    return Operation(var, func, args)

"""
def parse_program(contents):
    operations = []
    for line in contents.splitlines():
        operation = parse_line(line)
        if operation:
            operations.append(operation)

    return Program(operations)


def parse(file_):
    with open(file_) as fp:
        return parse_program(fp.read())
"""