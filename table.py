import csv
from collections import OrderedDict
from btree import BTree
from hash import HashTable
import condition_parser
import operator as operator_

# _conv_numeric function - Converts numeric string to integer
# @param conditions - string which can be converted to integer
# @returns the integer value if possible, else the string
def _conv_numeric(string):
    try:
        return int(string)
    except ValueError:
        return string

# a Table class creates a new table object and stored the fields and data
class Table:
    #remove this
    #print_depth = 20

    # __init__ function - Creating and initialising a Table object.
    # @param self - reference of the Table.
    # @param fields - the fields in the table.
    # @param data - the data in the table.
    def __init__(self, fields=None, data=None):
        self.fields = fields
        self.data = data
        self.indexes = {}

    # __repr__ function - Returns the entire Table object in printable format.
    # @param self -  value in the table.
    # @returns the string representation of the object.
    def __repr__(self):
        #data = self.data[:self.print_depth]

        data = self.data

        max_widths = [0] * len(self.fields)

        for row in [self.fields] + data:

            for idx, elem in enumerate(row):
                try:
                    max_widths[idx] = max(max_widths[idx], len(str(elem)))
                except Exception as e:
                    print(idx, elem)
                    raise e

        lines = []
        lines.append(' | '.join([
            field.rjust(max_width)
            for field, max_width in zip(self.fields, max_widths)]))

        lines.append('-+-'.join([
            '-' * max_width  for max_width in max_widths]))

        for row in data:
            lines.append(' | '.join([
                str(elem).rjust(max_width)
                for elem, max_width in zip(row, max_widths)]))

        return '\n'.join(lines)

    # __str__ function - Returns the string format of the reference.
    # @param self -  value in the table.
    # @returns __repr__ function.
    def __str__(self):
        return repr(self)

    # load function - Reads the input file and stores in the table object.
    # @param self - The reference of the table where the data needs to be stored.
    # @param file_ - The file name that contains the data.
    def load(self, file_):
        with open(file_, newline="") as fp:
            contents = list(csv.reader(
                fp, delimiter="|", quoting=csv.QUOTE_MINIMAL))

        self.fields = contents[0]
        self.data = contents[1:]
        # convert if numeric field
        for i in range(len(self.data)):
            for j in range(len(self.data[0])):
                self.data[i][j] = _conv_numeric(self.data[i][j].strip())

    # dump function - Cretes a reference for thr file to write the data.
    # @param self - The reference of the table where the data needs to be stored.
    # @param file_ - The file name where the results need to be written into.
    def dump(self, file_):
        with open(file_, "w", newline="") as fp:
            self.dump_fp (fp)

    # dump_fp function - Writes the data into the file.
    # @param self - The reference of the table where the data needs to be stored.
    # @param file_ - The reference for thr file to write the data.
    def dump_fp(self, fp):
        writer = csv.writer(fp, delimiter="|", quoting=csv.QUOTE_MINIMAL)
        writer.writerow(self.fields)
        writer.writerows(self.data)

    # project function - Returns the project of the field in the table.
    # @param self - The reference of the table object.
    # @param *fields - The fields up on which projection needs to happen.
    # @returns projection of the field in the table.
    def project(self, *fields):
        field_indices = [self.fields.index(field) for field in fields]
        data = [
            [row[idx] for idx in field_indices]
            for row in self.data
        ]
        return Table(fields, data)

    # concat function - Returns the concatenation function of two tables.
    # @param self - The reference of the first table object.
    # @param other - The reference of the second table object.
    # @returns concatenation of the two tables.
    def concat(self, other):
        if self.fields == other.fields:
            fields = self.fields
            data = self.data + other.data
            return Table(fields, data)
        else :
            return Table(None, None)
    
    # avg function - Returns the average of the field in table.
    # @param self - The reference of the table object.
    # @param field - The field up on which average function needs to be performed.
    # @returns average of the field in the table.
    def avg(self, field):
        field_idx = self.fields.index(field)
        return Table([field], [[sum(row[field_idx] for row in self.data) / len(self.data)]])

    # count function - Returns the count of the table.
    # @param self - The reference of the table object.
    # @returns count of the table.
    def count(self):
        return Table(['count'], [[len(self.data)]])

    # avg sum - Returns the sum of the field in table.
    # @param self - The reference of the table object.
    # @param field - The field up on which sum function needs to be performed.
    # @returns sum of the field in the table.
    def sum(self, field):
        field_idx = self.fields.index(field)
        return Table([field], [[sum(row[field_idx] for row in self.data)]])

    # sumgroup function - Returns the sumgroup of a field in the set of group fields in the table.
    # @param self - The reference of the table object.
    # @param sum_field - The field up on which sum needs to be performed.
    # @param *grp_fields - The fields up on which grouping needs to be performed.
    # @returns sumgroup function in the table.
    def sumgroup(self, sum_field, *grp_fields):
        grp_indices = [self.fields.index(field) for field in grp_fields]
        sum_idx = self.fields.index(sum_field)

        group_sum = OrderedDict()

        for row in self.data:
            group = tuple(row[idx] for idx in grp_indices)
            if group in group_sum:
                group_sum[group] += row[sum_idx]
            else:
                group_sum[group] = row[sum_idx]

        fields = list(grp_fields) + [f"sum({sum_field})"]
        data = [
            list(group) + [sum_]
            for group, sum_ in group_sum.items()
        ]
        return Table(fields, data)

    # countgroup function - Returns the countgroup of a field in the set of group fields in the table.
    # @param self - The reference of the table object.
    # @param *grp_fields - The fields up on which grouping needs to be performed.
    # @returns countgroup function in the table.
    def countgroup(self, *grp_fields):
        grp_indices = [self.fields.index(field) for field in grp_fields]

        group_count = OrderedDict()
        for row in self.data:
            group = tuple(row[idx] for idx in grp_indices)
            if group in group_count:
                group_count[group] += 1
            else:
                group_count[group] = 1

        fields = list(grp_fields) + ["count"]
        
        data = [
            list(group) + [count_]
            for group, count_ in group_count.items()
        ]
        print (fields)
        return Table(fields, data)

    # avggroup function - Returns the avggroup function of a field in the set of group fields in the table.
    # @param self - The reference of the table object.
    # @param avg_field - The field up on which average needs to be performed.
    # @param *grp_fields - The fields up on which grouping needs to be performed.
    # @returns avggroup function in the table.
    def avggroup(self, avg_field, *grp_fields):
        grp_indices = [self.fields.index(field) for field in grp_fields]
        sum_idx = self.fields.index(avg_field)

        group_sum_count = OrderedDict()

        for row in self.data:
            group = tuple(row[idx] for idx in grp_indices)
            if group in group_sum_count:
                sum_, count = group_sum_count[group]
                group_sum_count[group] = (sum_ + row[sum_idx], count + 1)
            else:
                group_sum_count[group] = (row[sum_idx], 1)

        fields = list(grp_fields) + [f"avg({avg_field})"]
        data = [
            list(group) + [sum_ / count]
            for group, (sum_, count) in group_sum_count.items()
        ]
        return Table(fields, data)

    # sort function - Returns the sorted version of a field in the table.
    # @param self - The reference of the table object.
    # @param field - The fields up on which the table needs to be sorted.   
    # @returns sort table up on the field.   
    def sort(self, *fields):
        for field in fields:
            field_idx = self.fields.index(field)
            self.data.sort(key=lambda row: row[field_idx])
        return self

    # movavg function - Returns the moving average of a field and length in the table.
    # @param self - The reference of the table object.
    # @param field - The field up on which moving average needs to be performed.
    # @param length - The number of items on which average needs to be taken.
    # @returns avggroup function in the table.   
    def movavg(self, field, length):
        field_idx = self.fields.index(field)
        length = _conv_numeric(length)
        data = []
        sum_ = 0
        for row_idx, row in enumerate(self.data):
            sum_ += row[field_idx]
            if row_idx >= length:
                sum_ -= self.data[row_idx - length][field_idx]
            data.append([sum_ / min(length, row_idx + 1)])
        fields = [f"movavg({field})"]

        return Table(fields, data)

    # movsum function - Returns the moving sum function of a field and length in the table.
    # @param self - The reference of the table object.
    # @param field - The field up on which moving sum needs to be performed.
    # @param length - The number of items on which sum needs to be calculated.
    # @returns movsum function in the table.
    def movsum(self, field, length):
        field_idx = self.fields.index(field)
        length = _conv_numeric(length)
        data = []
        sum_ = 0
        for row_idx, row in enumerate(self.data):
            sum_ += row[field_idx]
            if row_idx >= length:
                sum_ -= self.data[row_idx - length][field_idx]
            data.append([sum_])
        fields = [f"movsum({field})"]
        return Table(fields, data)
    
    # Btree function - Initialises a BTree with indexes of the field.
    # @param self - The reference of the table object.
    # @param field - The field up on which the indices needs to be stored.
    def Btree(self, field):
        tree = BTree(field, self)
        self.indexes[field] = tree
    # Hash function - Initialises a HashTable with indexes of the field.
    # @param self - The reference of the table object.
    # @param field - The field up on which the indices needs to be stored.
    def Hash(self, field):
        hash_table = HashTable(field, self)
        self.indexes[field] = hash_table

    # select function - Returns the select result on the conditions given.
    # @param self - The reference of the table object.
    # @param condtion - The condition up on which selection needs to be performed.
    # @returns the resulf of select operation.
    def select(self, condition_str):
        result = []
        rel_op, conditions = condition_parser.parse_condition(condition_str)
        for idx, condition in enumerate(conditions):
            if isinstance(condition[1], condition_parser.Value):
                conditions[idx] = condition_parser.flip_condition(condition)

        brute_conditions = []
        index_eq_conditions = []
        # find indexed equality conditions first to reduce search space
        for condition in conditions:
            comp_op, left_operand, _ = condition
            if comp_op is operator_.eq and left_operand.field in self.indexes:
                index_eq_conditions.append(condition)
            else:
                brute_conditions.append(condition)

        if rel_op in [None, "and"]:
            search_space = set(range(len(self.data))) # all rows
            for condition in index_eq_conditions:
                _, field_operand, value_operand = condition
                # invert the arith operation to the other side
                comparison_value = condition_parser.invert_arith(field_operand.arith_op)(value_operand.val, field_operand.arith_val)
                rows = self.indexes[field_operand.field].search(comparison_value)
                search_space.intersection_update(set(rows))

            for row_idx in sorted(search_space):
                row = self.data[row_idx]
                ok = True
                for condition in brute_conditions:
                    comp_op, field_operand, value_operand = condition
                    comparison_value = condition_parser.invert_arith(field_operand.arith_op)(value_operand.val, field_operand.arith_val)
                    field_idx = self.fields.index(field_operand.field)
                    if not comp_op(row[field_idx], comparison_value):
                        ok = False
                        break
                if ok:
                    result.append(row)

        elif rel_op == "or":
            included_rows = set()
            for condition in index_eq_conditions:
                _, field_operand, value_operand = condition
                # invert the arith operation to the other side
                comparison_value = condition_parser.invert_arith(field_operand.arith_op)(value_operand.val, field_operand.arith_val)
                rows = self.indexes[field_operand.field].search(comparison_value)
                included_rows = included_rows.union(set(rows))
            
            for row_idx in range(len(self.data)):
                if row_idx in included_rows:
                    continue
                row = self.data[row_idx]
                for condition in brute_conditions:
                    comp_op, field_operand, value_operand = condition
                    comparison_value = condition_parser.invert_arith(field_operand.arith_op)(value_operand.val, field_operand.arith_val)
                    field_idx = self.fields.index(field_operand.field)
                    if comp_op(row[field_idx], comparison_value):
                        included_rows.add(row_idx)
                        break
            
            for row_idx in sorted(included_rows):
                result.append(self.data[row_idx])

        return Table(self.fields, result)