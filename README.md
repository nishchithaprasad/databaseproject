# Database Final Project - A miniature relational database with order

A miniature relational database implemented in python with indexes based on BTrees and Hash.

# Language used: 
Python

# Libraries used:
1. BTrees.OBTree - Btrees: Installation required. Command given below.

2. Time - InBuilt python library.

3. csv - InBuilt python library.

4. Hash - InBuilt python library.

5. collection - InBuilt python library.

6. itertools - InBuilt python library.

# Installation Required:

```bash
pip3 install BTrees
```

# Execution file: 
main.py: The main.py file needs to be executed, with appropriate commands file name (If changed) and output file name (If changed).

# Input files:
The input files need to be added in input_files folder. 

1. commands.txt - The commands need to be in the file. The format for each command is there in test/sample_command.txt file. The whitespaces do not matter.

2. Data files - All the necessary data and input files need to be in test folder (test/sample.csv).

# Output files:
All the output files will be in the folder: output_files. The following are the output files:

1. nhv215_as13593_AllOperations: The outputs of all the operation are written into this file

2. nhv215_as13593_{output_to_file_filename}: The specific table output required in the commands.txt file.