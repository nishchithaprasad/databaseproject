from cmd_parser import parse_line

# run_file function - Here the commands are taken from the input file and parsed. The appropriate action is performed 
# and the result of each operation is written to the output file.
# @param input_file - The file with all the commands and data on which the operations have to be performed.
# @param output_file - The file into which results of each opertion is written.
# the namespace object contains the operations, tables after each operation and the result after each opeartion.
def run_file(input_file, output_file):
    with open(input_file) as fp:
        contents = fp.read()
    with open(output_file, "w") as fp1:
        #return ()
        namespace = {}
        for line in contents.splitlines():
            operation = parse_line(line)
            if operation == None:
                continue
            result = operation.run(namespace)
            if operation.var:
                namespace[operation.var] = result
            fp1.write(str(operation)+"\n")
            if result:
                result.dump_fp(fp1)