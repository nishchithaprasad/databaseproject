import interface
import time

# Operation class - Runs each operation and stores in the namespace dictionary woth the time taken. 
class Operation:
    # __init__ function - Initialises each operation with table name, function and arguments required.
    # @param self - Reference of the table.
    # @param var - Table name.
    # @param func - Function to be performed.
    # @param args - Arguments for the function.
    def __init__(self, var, func, args):
        self.var = var
        self.func = func
        self.args = args

    # __repr__ function - Appropriate function with arguments is evaluated.
    # @param self - Reference to the table.
    # @return The evaluated function is returned.
    def __repr__(self):
        args = ', '.join(self.args)
        if self.var:
            return f"{self.var} := {self.func}({args})"
        else:
            return f"{self.func}({args})"

    # __str__ function - printable representation returned.
    # @param self - Reference to the table.
    # @return printable representation is returned.
    def __str__(self):
        return repr(self)

    # run function - Appropriate function with arguments is called 
    # and the time taken for each operation is evaluated.
    # @param self - Reference to the table.
    # @param namespace - The object that holds all the tables and indixes.
    # @return result of each operation.
    def run(self, namespace):
        print(self, end=' ')

        start_time = time.time()
        result = interface.__dict__[self.func](namespace, *self.args)
        end_time = time.time()

        print(f"[Completed in {end_time - start_time:.2f} seconds]")
        print()
        print(result)
        print()
        print()

        return result

"""
class Program:
    def __init__(self, operations):
        self.namespace = {}
        self.operations = operations

    def __repr__(self):
        return "\n".join(
            map(repr, self.operations))

    def run(self):
        for operation in self.operations:
            result = operation.run(self.namespace)
            if operation.var:
                self.namespace[operation.var] = result
"""         